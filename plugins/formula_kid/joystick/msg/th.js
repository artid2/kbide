Blockly.Msg.JOYSTICK_1_POSITION_TITLE = "ตำแหน่งจอยสติ๊ก 1";
Blockly.Msg.JOYSTICK_1_POSITION_TOOLTIP = "อ่านตำแหน่งจอยสติ๊ก 1 (มีค่าตั้งแต่ -100 ถึง 100, ตำแหน่งกลางมีค่าเป็น 0)";
Blockly.Msg.JOYSTICK_1_POSITION_HELPURL = "";

Blockly.Msg.JOYSTICK_2_POSITION_TITLE = "ตำแหน่งจอยสติ๊ก 2";
Blockly.Msg.JOYSTICK_2_POSITION_TOOLTIP = "อ่านตำแหน่งจอยสติ๊ก 2 (มีค่าตั้งแต่ -100 ถึง 100, ตำแหน่งกลางมีค่าเป็น 0)";
Blockly.Msg.JOYSTICK_2_POSITION_HELPURL = "";
